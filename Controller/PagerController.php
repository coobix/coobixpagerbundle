<?php

namespace Coobix\PagerBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Coobix\PagerBundle\Model\Pager;


class PagerController extends Controller
{
    
    /**
     * List Pagination
     */
    public function pagerAction($pager, $template = NULL) {
     	if($template === NULL) {
            $template = 'CoobixPagerBundle:Pager:show.html.twig';
        }
    	return $this->render($template, array(
    			'pager' => $pager,
    			
    		
    	));
    
    }
}
