<?php

namespace Coobix\PagerBundle\Model;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\Tools\Pagination\Paginator;

class Pager {

    private $sfRequest;
    protected $page;
    protected $items;
    protected $numItems;
    protected $numPages;
    protected $pageStart;
    protected $pageEnd;
    protected $maxItemsPerPage = 10;
    protected $listUrlParams = [];

    public function __construct($query) {
        $this->sfRequest = Request::createFromGlobals();
        $this->setNumItems($query);
        $this->setPage();
        $this->setNumPages();
        $this->setPageStart();
        $this->setPageEnd();
    }

    public function setMaxItemsPerPage($maxItemsPerPage = 0) {
        $this->maxItemsPerPage = $maxItemsPerPage;
        
        return $this;
    }

    public function getMaxItemsPerPage() {
        if ($this->sfRequest->query->has("_limit")) {

            $this->maxItemsPerPage = (($this->sfRequest->query->get("_limit")) == 0) ? $this->getNumItems() : ($this->sfRequest->query->get("_limit"));
        } 
        return $this->maxItemsPerPage;
    }

    public function setNumItems($query) {
        $paginator = new Paginator($query, true);
        $paginator->setUseOutputWalkers(false);
        $this->numItems = count($paginator);
    }

    public function getNumItems() {
        return $this->numItems;
    }

    public function setPage() {
        if ($this->sfRequest->query->has("_page")) {
            $this->page = $this->sfRequest->query->get("_page");
        } else {
            $this->page = 1;
        }

        return $this;
    }

    public function getPage() {
        return $this->page;
    }

    public function setNumPages() {
        $numPages = ceil($this->getNumItems() / $this->getMaxItemsPerPage());
        $this->numPages = $numPages;
    }

    public function getNumPages() {
        return $this->numPages;
    }

    public function setPageStart() {
        $this->pageStart = (($this->getPage() - 3) < 1) ? 1 : ($this->getPage() - 3);
    }

    public function getPageStart() {
        return $this->pageStart;
    }

    public function setPageEnd() {
        $this->pageEnd = (($this->getPage() + 3) < $this->getNumPages()) ? ($this->getPage() + 3) : $this->getNumPages();
    }

    public function getPageEnd() {
        return $this->pageEnd;
    }

    public function getPagerUrl($page) {
        $urlGetsParams = $this->sfRequest->query->all();
        
        $urlGetsParams['_page'] = $page;

        $this->listUrlParams = array_merge($this->listUrlParams, $urlGetsParams);

        return $this->getListUrl();
    }

    public function getListUrl() 
    {
        if (null !== $qs = http_build_query($this->listUrlParams)) {
            $qs = '?'.$qs;
        }
        return $this->sfRequest->getSchemeAndHttpHost().$this->sfRequest->getBaseUrl().$this->sfRequest->getPathInfo().$qs;
    }


}
