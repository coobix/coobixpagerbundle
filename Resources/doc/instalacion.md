Instalación de CoobixPagerBundle
==================================

## Prerequisitos

## Instalación

1. Descargar CoobixPagerBundle usando composer
2. Habilitar el bundle


### Paso 1: Descargar CoobixPagerBundle usando composer

Agregar el repositorio del bundle en composer.json:

``` bash
"repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:coobix/coobixpagerbundle.git"
        }
    ]
```

Agregar el bundle con el siguiente comando:

``` bash
$ composer require coobix/pager-bundle "dev-master"
```


Composer instalara el  bundle en el directorio `vendor/coobix`.

### Paso 2: Habilitar el bundle

``` php
<?php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
        new Coobix\PagerBundle\CoobixPagerBundle(),
    );
}
```

### Paso 3: Configurar el bundle

``` yaml
# app/config/services.yml
services:
    coobix.pager:
        class: Coobix\PagerBundle\Entity\Pager
        arguments: 
            - "@router"
```

### Paso 4: Importar archivos de rutas

YAML:

``` yaml
# app/config/routing.yml
coobix_pager:
    resource: "@CoobixPagerBundle/Resources/config/routing.yml"
    prefix:   /
```

